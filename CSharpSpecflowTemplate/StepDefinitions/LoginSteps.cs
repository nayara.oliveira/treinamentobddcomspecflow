﻿using CSharpSpecflowTemplate.Flows;
using CSharpSpecflowTemplate.Helpers;
using CSharpSpecflowTemplate.Pages;
using System;
using System.Configuration;
using TechTalk.SpecFlow;

namespace CSharpSpecflowTemplate.StepDefinitions
{
    [Binding]
    public class LoginSteps
    {

        LoginFlows loginFlows;
        LoginPage loginPage;

        public LoginSteps()
        {
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
        }

        [StepDefinition(@"que o usuário esteja na página de login")]
        public void GivenQueOUsuarioEstejaNaPaginaDeLogin()
        {
            DriverFactory.INSTANCE.Navigate().GoToUrl(ConfigurationManager.AppSettings["base_url"]);
        }

        [StepDefinition(@"o usuário informar um usuario valido")]
        public void WhenOUsuarioInformarUmUsuarioValido()
        {
            loginPage.PreencherUsuario(ConfigurationManager.AppSettings["username"]);
        }

        [StepDefinition(@"informar uma senha válida")]
        public void WhenInformarUmaSenhaValida()
        {
            loginPage.PreencherSenha(ConfigurationManager.AppSettings["password"]);
        }
        [StepDefinition(@"efetuar login")]
        public void WhenEfetuarLogin()
        {
            loginPage.ClicarEmLogin();
        }

        [StepDefinition(@"o sistema irá mostrar uma mensagem de sucesso")]
        public void ThenOSistemaIraMostrarUmaMensagemDeSucesso()
        {
            loginPage.RetornarMensagemDeLoginComSucesso();
        }

        [StepDefinition(@"o usuário informar um usuario inválido")]
        public void WhenOUsuarioInformarUmUsuarioInvalido()
        {
            loginPage.PreencherUsuario(ConfigurationManager.AppSettings["invalidUsername"]);
        }

        [StepDefinition(@"informar uma senha inválida")]
        public void WhenInformarUmaSenhaInvalida()
        {
            loginPage.PreencherUsuario(ConfigurationManager.AppSettings["invalidPassword"]);
        }

        [StepDefinition(@"o sistema irá mostrar uma mensagem de erro")]
        public void ThenOSistemaIraMostrarUmaMensagemDeErro()
        {
            loginPage.RetornarMensagemDeErro();
        }

    }
}
