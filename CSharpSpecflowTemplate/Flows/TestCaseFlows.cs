﻿using CSharpSpecflowTemplate.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Flows
{
    public class TestCaseFlows
    {

        TestCasePage testCasePage;

        #region Page Objects and Flows
        public TestCaseFlows()
        {
            testCasePage = new TestCasePage();
        }
        #endregion

        #region
        public void AcessarCasoDeTeste()
        {
            testCasePage.ClicarEmCasosDeTeste();

        }
        #endregion
    }
}
