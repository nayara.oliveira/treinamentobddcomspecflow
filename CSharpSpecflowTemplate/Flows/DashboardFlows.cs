﻿using CSharpSpecflowTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Flows
{
    public class DashboardFlows
    {
        DashboardPage dashboardPage;

        #region Page Objects and Flows
        public DashboardFlows()
        {
            dashboardPage = new DashboardPage();
        }
        #endregion

        public void AcessarPaginaDeProjetos()
        {
            dashboardPage.AcessarProjetos();
        }

    }
}
