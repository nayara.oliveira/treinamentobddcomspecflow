﻿using CSharpSpecflowTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Flows
{
    public class ProjectsFlows
    {
        ProjectsPage projectsPage = new ProjectsPage();

        #region Construtor
        public ProjectsFlows()
        {
            projectsPage = new ProjectsPage();
        }
        #endregion

        #region Flows
        public void AcessarPrimeiroProjeto()
        {
            projectsPage.ClicarEmPrimeiroProjeto();
        }
        public void EditarProjeto(string nomeProjeto)
        {
            projectsPage.ClicarEmEditarProjeto();
            projectsPage.PreencherNomeProjeto(nomeProjeto);
            projectsPage.SalvarAlteracao();
        }
        public void CriarNovoProjeto(string identificador, string nomeProjeto, string descricao, string tipo)
        {
            projectsPage.ClicarEmIncluir();
            projectsPage.PreencherIdentificador(identificador);
            projectsPage.PreencherNomeProjeto(nomeProjeto);
            projectsPage.PreencherDescricao(descricao);
            projectsPage.SelecionarTipoDeProjeto(tipo);
        }
        public void AcessarPaginaDeCasosDeTeste()
        {
            projectsPage.ClicarEmPrimeiroProjeto();
        }
        #endregion
    }
}
