﻿using CSharpSpecflowTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Flows
{
    public class MainFlows
    {
        MainPage mainPage;

        #region Page Objects and Flows
        public MainFlows()
        {
            mainPage = new MainPage();
        }
        #endregion

        public void GerenciarProjetos()
        {
            mainPage.ClicarEmGerenciarProjetos();
        }
    }
}
