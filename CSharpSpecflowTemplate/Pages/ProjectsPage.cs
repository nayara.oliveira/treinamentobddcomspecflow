﻿using CSharpSpecflowTemplate.Bases;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Pages
{
    class ProjectsPage : PageBase
    {

        #region Mapping Editar Projeto
        private By primeiroProjetoTableCellText = By.XPath("//td[3][contains(text(),'1')]  ");
        private By editarProjetoButton = By.XPath("//a[@class='ppt-btn-default ppt-btn-edit']");
        private By nomeDoProjetoField = By.XPath("//input[@id='project_name']");
        private By alterarProjetoButton = By.Id("btn-save-project");
        private By mensagemConfirmaAlteracaoAlert = By.XPath("/html/body/div[2]/div/div[2]/div/span");
        private By mensagemErroIdentificadorIgualAlert = By.Id("elementErrorMsg_project_identifier");
        #endregion

        #region Mapping Criar Projeto
        private By incluirField = By.Id("btn-add");
        private By identificadorField = By.Id("project_identifier");
        private By descricaoField = By.Id("project_description");
        private By tipoSelect = By.Id("project_project_type_id");
        private By mensagemConfirmaCriacaoAlert = By.XPath("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']");
        #endregion


        #region Actions Editar Projeto
        public void ClicarEmPrimeiroProjeto()
        {
            Click(primeiroProjetoTableCellText);
        }
        public void ClicarEmEditarProjeto()
        {
            Click(editarProjetoButton);
        }

        public void PreencherNomeProjeto(string nome)
        {
            ClearAndSendKeys(nomeDoProjetoField, nome);
        }

        public void SalvarAlteracao()
        {
            Click(alterarProjetoButton);
        }

        public string RetornarMensagemDeAlteracaoComSucesso()
        {
            return GetText(mensagemConfirmaAlteracaoAlert);
        }

        public string RetornarMensagemDeCriacaoComSucesso()
        {
            return GetText(mensagemConfirmaCriacaoAlert);
        }

        public bool RetornarMensagemDeErroIdentificadorIgual()
        {
            return ReturnIfElementIsEnabled(mensagemErroIdentificadorIgualAlert);
        }

        public string RetornarNomeDoProjeto()
        {
            return GetText(nomeDoProjetoField);
        } 
        
        public void VerificarEdicaoDoNomeDoProjeto()
        {
            string a = RetornarMensagemDeCriacaoComSucesso();
            Assert.IsTrue(WaitForElement(mensagemConfirmaAlteracaoAlert).Text.Contains(a));
        }

        #region Criar Projeto
        public void ClicarEmIncluir()
        {
            Click(incluirField);
        }

        public void PreencherIdentificador(string identificador)
        {
            SendKeys(identificadorField, identificador);
        }

        public void PreencherDescricao(string descricao)
        {
            SendKeys(descricaoField, descricao);
        }

        public void SelecionarTipoDeProjeto(string index)
        {
            ComboBoxSelectByVisibleText(tipoSelect, index);
        }
        #endregion
        #endregion
    }
}


