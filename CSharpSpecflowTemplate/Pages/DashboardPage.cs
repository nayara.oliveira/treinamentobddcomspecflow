﻿using CSharpSpecflowTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Pages
{
    public class DashboardPage : PageBase
    {
        #region Mapping       
        private By projectsMenu = By.XPath("//*[@id='wrapper']/nav/div[2]/div/ul/a[2]/li");
        #endregion

        #region Actions
        public void AcessarProjetos()
        {
            Click(projectsMenu);
        }
        #endregion
    }
}
