using CSharpSpecflowTemplate.Bases;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSpecflowTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping
        private By usernameField = By.Id("user_email");
        private By passwordField = By.Id("user_password");
        private By loginButton = By.XPath("//input[@type='submit']");
        private By mensagemDeErro = By.XPath("//div[@class='alert alert-danger alert-dismissable']");
        private By mensagemConfirmaLogin = By.XPath("//div[@class='notifyjs-bootstrap-base notifyjs-bootstrap-success']");

        #endregion


        #region Actions
        public void PreencherUsuario(string usuario)
        {
            SendKeys(usernameField, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(passwordField, senha);
        }

        public void ClicarEmLogin()
        {

            WaitForElementIsVisible(loginButton).Click();
        }

        public void RetornarMensagemDeErro()
        {
            Assert.AreEqual("Usu�rio ou senha inv�lidos!", GetText(mensagemDeErro)); ;
        }

        public void RetornarMensagemDeLoginComSucesso()
        {
            Assert.AreEqual("Bem-vindo! nayara.oliveira", GetText(mensagemConfirmaLogin)); ;
        }
        #endregion
    }
}
