﻿using CSharpSpecflowTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpSpecflowTemplate.Pages
{
    class TestCasePage : PageBase
    {
        #region Mapping
        private By casoDeTesteBy = By.XPath("//*[@id='li-test-cases-link']");
        By identificadorBy = By.XPath("//td[2][1]");
        #endregion

        #region Actions
        public void ClicarEmCasosDeTeste()
        {
            WaitForElementToBeClickable(casoDeTesteBy).Click();
        }

        public string RetornarTextoDoIdentificador()
        {
            return driver.FindElement(identificadorBy).Text;
        }

    #endregion
}
}
