﻿Feature: Login

	As a Tester
	I Want To Fazer login no sistema
	In Order To acessar suas funcionalidades

	Background:
			Given que o usuário esteja na página de login

@login
	Scenario: Fazer login no sistema
		When o usuário informar um usuario valido
		And informar uma senha válida
		And efetuar login
		Then o sistema irá mostrar uma mensagem de sucesso

@loginSemSucesso
	Scenario: Obter erro ao tentar logar no sistema
		When o usuário informar um usuario inválido
		And informar uma senha inválida
		And efetuar login
		Then o sistema irá mostrar uma mensagem de erro