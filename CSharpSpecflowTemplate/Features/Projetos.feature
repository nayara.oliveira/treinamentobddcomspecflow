﻿Feature: Projetos

	As a dono dos projetos
	I want to visualizar os projetos
	In order to acessar recursos dos projetos
	
	Background: 
		Given que o usuário acesse o sistema crowdtest
		And efetuar o login
		And acessar a dashboard
		And acessar página de projetos

@editarNomeDoProjeto
	Scenario: Editar o nome do projeto de teste
		When o usuário acessar a página de um projeto
		And editar o nome desse projeto
		Then o sistema mostrará uma mensagem de projeto alterado com sucesso

@criarProjeto
	Scenario: Criar um Projeto
		When o usuário informar os dados de um novo projeto
		And salvar esses dados
		Then será mostrada uma mensagem de projeto criado com sucesso

@validarCasoDeTeste
	Scenario: Caso de teste existente
		When o usuário acessar a página de um projeto
		And acessar a página de casos de testes
		And procurar o primeiro caso de teste
		Then o caso de teste estará listado
