﻿using CSharpSpecflowTemplate.Flows;
using CSharpSpecflowTemplate.Pages;
using CSharpSpecflowTemplate.Helpers;
using System;
using System.Configuration;
using TechTalk.SpecFlow;
using NUnit.Framework;

namespace CSharpSpecflowTemplate
{
    [Binding]
    public class ProjetosSteps
    {
        LoginFlows loginFlows;
        ProjectsFlows projectsFlows;
        TestCaseFlows testCaseFlows;
        DashboardFlows dashboardFlows;
        MainFlows mainFlows;
        ProjectsPage projectsPage;
        TestCasePage testCasePage;
 
        string identificador =  "NC028";
        string nomeProjeto = "projeto_remotoNay8";
        string descricao = "projeto remoto da Nay";
        string tipo = "Web";
        string nomeCasoDeTeste = "CT01";

        public ProjetosSteps()
        {
            loginFlows = new LoginFlows();
            projectsFlows = new ProjectsFlows();
            dashboardFlows = new DashboardFlows();
            mainFlows = new MainFlows();
            testCaseFlows = new TestCaseFlows();
            projectsPage = new ProjectsPage();
            testCasePage = new TestCasePage();
        }


        [StepDefinition(@"que o usuário acesse o sistema crowdtest")]
        public void GivenQueOUsuarioAcesseOSistemaCrowdtest()
        {
            DriverFactory.INSTANCE.Navigate().GoToUrl(ConfigurationManager.AppSettings["base_url"]);
        }

        [StepDefinition(@"efetuar o login")]
        public void GivenEfetuarOLogin()
        {
            loginFlows.EfetuarLogin(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"]);

        }
        [StepDefinition(@"acessar a dashboard")]
        public void GivenAcessarADashboard()
        {
            mainFlows.GerenciarProjetos();
        }
        [StepDefinition(@"acessar página de projetos")]
        public void GivenAcessarPaginaDeProjetos()
        {
            dashboardFlows.AcessarPaginaDeProjetos();
        }

        [StepDefinition(@"o usuário acessar a página de um projeto")]
        public void WhenOUsuarioAcessarAPaginaDeUmProjeto()
        {
            projectsPage.ClicarEmPrimeiroProjeto();
        }
        
        [StepDefinition(@"editar o nome desse projeto")]
        public void WhenEditarONomeDesseProjeto()
        {
            projectsFlows.EditarProjeto(nomeProjeto);
        }
        
        [StepDefinition(@"o sistema mostrará uma mensagem de projeto alterado com sucesso")]
        public void ThenOSistemaMostraraUmaMensagemDeProjetoAlteradoComSucesso()
        {
            projectsPage.VerificarEdicaoDoNomeDoProjeto();

        }

        [StepDefinition(@"o usuário informar os dados de um novo projeto")]
        public void WhenOUsuarioInformarOsDadosDeUmNovoProjeto()
        {
            projectsFlows.CriarNovoProjeto(identificador, nomeProjeto, descricao, tipo);
        }
        
        [StepDefinition(@"salvar esses dados")]
        public void WhenSalvarEssesDados()
        {
            projectsPage.SalvarAlteracao();
        }
        
        [StepDefinition(@"será mostrada uma mensagem de projeto criado com sucesso")]
        public void ThenSeraMostradaUmaMensagemDeProjetoCriadoComSucesso()
        {
            projectsPage.RetornarMensagemDeCriacaoComSucesso();
        }

        [StepDefinition(@"acessar a página de casos de testes")]
        public void WhenAcessarAPaginaDeCasosDeTestes()
        {
            testCaseFlows.AcessarCasoDeTeste();
        }

        [StepDefinition(@"procurar o primeiro caso de teste")]
        public void WhenProcurarOPrimeiroCasoDeTeste()
        {
            testCasePage.RetornarTextoDoIdentificador();
        }

        [StepDefinition(@"o caso de teste estará listado")]
        public void ThenOCasoDeTesteEstaraListado()
        {
            Assert.AreEqual(nomeCasoDeTeste, testCasePage.RetornarTextoDoIdentificador());
        }

    }
}
