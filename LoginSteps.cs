﻿using System;
using CSharpSpecflowTemplate.Flows;
using CSharpSpecflowTemplate.Helpers;
using CSharpSpecflowTemplate.Pages;
using System.Configuration;
using TechTalk.SpecFlow;
namespace CSharpSpecflowTemplate
{
    [Binding]
    public class LoginSteps
    {
        LoginFlows loginFlows;
        LoginPage loginPage;

        public LoginSteps()
        {
            loginFlows = new LoginFlows();
            loginPage = new LoginPage();
        }

        [StepDefinition(@"que eu esteja na página de login")]
        public void GivenQueEuEstejaNaPaginaDeLogin()
        {
            DriverFactory.INSTANCE.Navigate().GoToUrl(ConfigurationManager.AppSettings["base_url"]);
        }

        [StepDefinition(@"eu informo o usuario")]
        public void WhenEuInformoOUsuario()
        {
            loginPage.PreencherUsuario(ConfigurationManager.AppSettings["username"]);
        }

        [StepDefinition(@"informo a senha")]
        public void WhenInformoASenha()
        {
            loginPage.PreencherSenha(ConfigurationManager.AppSettings["password"]);
        }

        [StepDefinition(@"clico em logar")]
        public void WhenClicoEmLogar()
        {
            loginPage.ClicarEmLogin();
        }

        [StepDefinition(@"será mostrada a mensagem de sucesso")]
        public void ThenSeraMostradaAMensagemDeSucesso()
        {
            loginPage.RetornarMensagemDeLoginComSucesso();
        }

        [StepDefinition(@"eu efetuo login")]
        public void WhenEuEfetuoLogin()
        {

            loginFlows.EfetuarLogin(ConfigurationManager.AppSettings["invalidUsername"], ConfigurationManager.AppSettings["invalidPassword"]);
        }

        [StepDefinition(@"será mostrada a mensagem de erro")]
        public void ThenSeraMostradaAMensagemDeErro()
        {
            loginPage.RetornarMensagemDeErro();
        }
    }
}
